# YanniWorks

[![CI Status](http://img.shields.io/travis/Ioannis Pasmatzis/YanniWorks.svg?style=flat)](https://travis-ci.org/Ioannis Pasmatzis/YanniWorks)
[![Version](https://img.shields.io/cocoapods/v/YanniWorks.svg?style=flat)](http://cocoapods.org/pods/YanniWorks)
[![License](https://img.shields.io/cocoapods/l/YanniWorks.svg?style=flat)](http://cocoapods.org/pods/YanniWorks)
[![Platform](https://img.shields.io/cocoapods/p/YanniWorks.svg?style=flat)](http://cocoapods.org/pods/YanniWorks)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YanniWorks is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YanniWorks"
```

## Author

Ioannis Pasmatzis, johnpasma@hotmail.com

## License

YanniWorks is available under the MIT license. See the LICENSE file for more info.
