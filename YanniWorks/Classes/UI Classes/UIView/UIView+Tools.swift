//
//  UIView+Tools.swift
//  YanniWorks
//
//  Created by IOANNIS PASMATZIS on 12/5/16.
//  Copyright © 2016 Ioannis Pasmatzis. All rights reserved.
//

import UIKit

extension UIView {
    
    //MARK: Sizing
    
    public var width: CGFloat {
        get { return frame.width }
        set { frame.size.width = newValue }
    }
    
    public var height: CGFloat {
        get { return frame.height }
        set { frame.size.height = newValue }
    }

    public func set(_ width: CGFloat, height: CGFloat) {
        self.width = width
        self.height = height
    }
    
    public func set(_ left: CGFloat, top: CGFloat) {
        self.left = left
        self.top = top
    }
    
    //MARK: Positioning
    
    public var top: CGFloat {
        get { return frame.origin.y }
        set { frame.origin.y = newValue }
    }
    
    public var left: CGFloat {
        get { return frame.origin.x }
        set { frame.origin.x = newValue }
    }
    
    public var right: CGFloat {
        get { return frame.origin.x + width }
        set { frame.origin.x = newValue - width }
    }
    
    public var bottom: CGFloat {
        get { return frame.origin.y + height }
        set { frame.origin.y = newValue - height }
    }
    
    public func centerHorizontaly() {
        
        guard let parent = superview else { return }
        
        self.left = (parent.width - width) * 0.5
    }
    
    public func centerVertically() {
        
        guard let parent = superview else { return }
        
        self.top = (parent.height - height) * 0.5
    }
    
    public func centerInParent() {
        centerVertically()
        centerHorizontaly()
    }

    //MARK: Setters with other Views
    
    public func placeRightOf(_ view: UIView, _ padding: CGFloat = 0) {
        
        left = view.right
    }
    
    public func placeLeftOf(_ view: UIView, _ padding: CGFloat = 0) {
        
        right = view.left
    }
    
    public func placeAbove(_ view: UIView, _ padding: CGFloat = 0) {
        
        bottom = view.top
    }
    
    public func placeBelow(_ view: UIView, _ padding: CGFloat = 0) {
        
        top = view.bottom
    }

}
