//
//  UIView+Initializers.swift
//  YanniWorks
//
//  Created by IOANNIS PASMATZIS on 12/5/16.
//  Copyright © 2016 Ioannis Pasmatzis. All rights reserved.
//

import UIKit
import CoreGraphics

extension UIView {
    
    convenience init(width: CGFloat, height:CGFloat) {
        
        self.init()
        
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        self.frame = frame
    }
    
    convenience init(width: CGFloat, height: CGFloat, left: CGFloat, top: CGFloat) {
        
        self.init()
        
        let frame = CGRect(x: left, y: top, width: width, height: height)
        
        self.frame = frame
    }
}
