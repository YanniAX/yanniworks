#
# Be sure to run `pod lib lint YanniWorks.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YanniWorks'
  s.version          = '0.1.3'
  s.summary          = 'My iOS Framework'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
    This is my custom framework for iOS apps. Nothing too special, just some basic functionality that I
    want to use in all my apps.
                       DESC

  s.homepage         = 'https://bitbucket.org/YanniAX/yanniworks'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Ioannis Pasmatzis' => 'johnpasma@hotmail.com' }
  s.source           = { :git => 'https://YanniAX@bitbucket.org/YanniAX/yanniworks.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'YanniWorks/Classes/**/*'
  
  # s.resource_bundles = {
  #   'YanniWorks' => ['YanniWorks/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
